# SideChef CXP SDK for Android

This project is CXP SDK for Android, the demo in https://gitlab.com/sidechef/SideChefCXP_Android_Demo

### Requirements

- JDK 11
- Android min SDK 27

### Usage

1. Add `aar` file to `libs` directory. Get `aar` file form SideChef or copy from this project in `libs` directory.

2. Add this code in `build.gradle` file
   
   ```groovy
   implementation fileTree(dir: 'libs', include: ['*.jar','*.aar'])

   // optional for use SCAPI
   implementation 'com.squareup.retrofit2:retrofit:2.3.0' 
   implementation 'com.squareup.retrofit2:converter-gson:2.3.0'

   ```
   
3. Initializing the SDK on your Application onCreate:

   ```java
   import com.sidechef.cxp.UserInfo;
   import com.sidechef.cxp.SideChefXCP;

   // provided by SideChef
   String apiKey = "bGc6WHVLNHE1c21YQm5XcmU5S3k2bnVhVVNOTlMzQ05M";
   // provided by your app
   UserInfo userInfo = new UserInfo("user_id", "https://example.com/images/me.jpg");
   
   SideChefXCP.getInstance().configure(this, apiKey, userInfo);
   
   // set appliances inside configure
   String[] appliances = ['lg' 'aeg'];
   SideChefXCP.getInstance().configure(this, apiKey, userInfo, appliances);

   // or you can set appliances with 'setAppliances'
    SideChefXCP.getInstance().setAppliances(appliances);
   ```

4. Usage of SCWebView

   Use in a class view:

   ```java
   import androidx.appcompat.app.AlertDialog;
   import com.sidechef.cxp.SCWebView;
   import com.sidechef.cxp.SCAPI;
   import com.sidechef.cxp.ProgressHelper;
   

   SCWebView webView = new SCWebView(this);

   // add loading progress view as optional
   AlertDialog.Builder builder = new AlertDialog.Builder(this);
   builder.setMessage("Custom ProgressView");
   AlertDialog dialog = builder.create();
   
   webView.setProgressView(dialog);
   
   // or use ProgressHelper create AlertDialog
   //   AlertDialog dialog = ProgressHelper.getProgressDialog(context, "message");
   //   webView.setProgressView(dialog);

   // set onExternalLink  
   webView.setOnExternalLink(new SCWebView.Callback() {
       @Override
       public void execute(String url) {
            // do stuff.
       }
   });

   // setInitialPage for recipe with id 
   webView.setInitialPage(SCInitialPage.recipe, "12");

   // or setInitialPage for mealPlan
   // webView.setInitialPage(SCInitialPage.mealPlan);

   containerView.addView(webView);
   ```

   Use in XML layout:

   ```xml
   <com.sidechef.cxp.SCWebView
      android:id="@+id/scwebview"
      android:layout_width="match_parent"
      android:layout_height="match_parent"
      app:hasProgress="false" // hide progress in xml as optional
   />

5. Usage of SCAPI:

   ```java
   import com.sidechef.cxp.SCAPI;
   import com.sidechef.cxp.network.ListResponse;
   import com.sidechef.cxp.network.Recipe;
   
   import retrofit2.Call;
   import retrofit2.Callback;
   import retrofit2.Response;
   
   int page = 1;
   int pageSize = 5;
   SCAPI.getInstance().getHomePopular(page, pageSize, new Callback<ListResponse<Recipe>>() {
       @Override
       public void onResponse(Call<ListResponse<Recipe>> call, Response<ListResponse<Recipe>> response) {
           // ...
       }

       @Override
       public void onFailure(Call<ListResponse<Recipe>> call, Throwable t) {
           // ...
       }
   });
   ```

### API

`apiKey`: String - The API key provided by SideChef.

`userInfo`: UserInfo - The user object object provided by the host app.Provided by your app

- `id`: String - The user identifier.
- `photo`: String (optional) - The photo of the user's avatar.

`SCInitialPage`: open SideChef on any of our URLs page
We support:
- `recipe`: SCInitialPage - open a recipe with id.
- `mealPlan`: SCInitialPage -  go to meal plan.
- `grocery`: SCInitialPage -  go to grocery.

### `SCWebView` props
| Property | Description | Type | Required | Default |
| :--- | :--- | :---: | :---: | :---: |
| `hasProgress` | Display a progress indicator bar on the top of the view | boolean | no | `true` |
| `progressView` | show loading progress dialog  | AlertDialog | no | AlertDialog with message: 'Loading...' |
| `onExternalLink` | A function that is called before following a link  | SCWebView.Callback | no | undefined |
| `initialPage` | open SideChef on any of our URLs  | SCInitialPage | no | undefined |
