package com.sidechef.cxp.network;

import java.util.Map;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.QueryMap;

public interface API {

    @GET("home/popular/")
    Call<ListResponse<Recipe>> getHomePopular(@QueryMap Map<String, Object> map);

    @POST("account/signup/?redirect=1")
    Call<ResponseBody> authenticate(@Body Map<String, Object> map);
}
