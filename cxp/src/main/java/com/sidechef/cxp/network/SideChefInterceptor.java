package com.sidechef.cxp.network;

import android.util.Log;

import com.sidechef.cxp.SideChefCXP;
import com.sidechef.cxp.Utils;

import java.io.IOException;
import java.util.List;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class SideChefInterceptor implements Interceptor {

    private static final String TAG = "SideChefInterceptor";

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request request = chain.request();
        Request newRequest;
        Request.Builder requestBuilder = request.newBuilder();

        requestBuilder
                .removeHeader("User-Agent")
                .addHeader("User-Agent", Utils.getUserAgent());

        String cookies = SideChefCXP.getInstance().getCookie();
        if (!cookies.isEmpty()) {
            requestBuilder.addHeader("Cookie", cookies);
            Log.d(TAG, "using current cookies");
        }

        newRequest = requestBuilder.build();
        return chain.proceed(newRequest);
    }
}
