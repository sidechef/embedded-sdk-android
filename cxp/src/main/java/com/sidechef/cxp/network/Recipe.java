package com.sidechef.cxp.network;

import java.util.Arrays;

public class Recipe {
    private int id;
    private String name;
    private Float avg_rating;
    private int calories_per_serving;
    private int cook_time;
    private String[] countries;
    private String cover_pic_url;
    private String cover_pic_url_origin;
    private String cover_pic_url_small;
    private String default_price_in_cart;
    private String[] dish_tags;
    private String[] dish_types;
    private int display_rating;
    private String display_servings;
    private String display_time;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Float getAvg_rating() {
        return avg_rating;
    }

    public void setAvg_rating(Float avg_rating) {
        this.avg_rating = avg_rating;
    }

    public int getCalories_per_serving() {
        return calories_per_serving;
    }

    public void setCalories_per_serving(int calories_per_serving) {
        this.calories_per_serving = calories_per_serving;
    }

    public int getCook_time() {
        return cook_time;
    }

    public void setCook_time(int cook_time) {
        this.cook_time = cook_time;
    }

    public String[] getCountries() {
        return countries;
    }

    public void setCountries(String[] countries) {
        this.countries = countries;
    }

    public String getCover_pic_url() {
        return cover_pic_url;
    }

    public void setCover_pic_url(String cover_pic_url) {
        this.cover_pic_url = cover_pic_url;
    }

    public String getCover_pic_url_origin() {
        return cover_pic_url_origin;
    }

    public void setCover_pic_url_origin(String cover_pic_url_origin) {
        this.cover_pic_url_origin = cover_pic_url_origin;
    }

    public String getCover_pic_url_small() {
        return cover_pic_url_small;
    }

    public void setCover_pic_url_small(String cover_pic_url_small) {
        this.cover_pic_url_small = cover_pic_url_small;
    }

    public String getDefault_price_in_cart() {
        return default_price_in_cart;
    }

    public void setDefault_price_in_cart(String default_price_in_cart) {
        this.default_price_in_cart = default_price_in_cart;
    }

    public String[] getDish_tags() {
        return dish_tags;
    }

    public void setDish_tags(String[] dish_tags) {
        this.dish_tags = dish_tags;
    }

    public String[] getDish_types() {
        return dish_types;
    }

    public void setDish_types(String[] dish_types) {
        this.dish_types = dish_types;
    }

    public int getDisplay_rating() {
        return display_rating;
    }

    public void setDisplay_rating(int display_rating) {
        this.display_rating = display_rating;
    }

    public String getDisplay_servings() {
        return display_servings;
    }

    public void setDisplay_servings(String display_servings) {
        this.display_servings = display_servings;
    }

    public String getDisplay_time() {
        return display_time;
    }

    public void setDisplay_time(String display_time) {
        this.display_time = display_time;
    }

    @Override
    public String toString() {
        return "Recipe{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", avg_rating=" + avg_rating +
                ", calories_per_serving=" + calories_per_serving +
                ", cook_time=" + cook_time +
                ", countries=" + Arrays.toString(countries) +
                ", cover_pic_url='" + cover_pic_url + '\'' +
                ", cover_pic_url_origin='" + cover_pic_url_origin + '\'' +
                ", cover_pic_url_small='" + cover_pic_url_small + '\'' +
                ", default_price_in_cart='" + default_price_in_cart + '\'' +
                ", dish_tags=" + Arrays.toString(dish_tags) +
                ", dish_types=" + Arrays.toString(dish_types) +
                ", display_rating=" + display_rating +
                ", display_servings='" + display_servings + '\'' +
                ", display_time='" + display_time + '\'' +
                '}';
    }
}
