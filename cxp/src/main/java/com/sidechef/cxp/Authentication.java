package com.sidechef.cxp;

import android.util.Base64;
import android.util.Log;

import androidx.annotation.NonNull;

import org.json.JSONException;
import org.json.JSONObject;

import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

public class Authentication {
    private static final String TAG = "Authentication";

    // hardcode for now, same as iOS & RN version
    public static final String BASE_URL = "https://stageapi.sidechef.com/";

    private static String[] getKeys() {
        String apiKey = SideChefCXP.getInstance().getApiKey();
        byte[] aa = Base64.decode(apiKey, Base64.DEFAULT);
        return new String(aa).split(":");
    }

    public static String getPartnerKey() {
        return getKeys()[0];
    }

    public static HashMap<SCInitialPage, String> getMappings() {
        HashMap<SCInitialPage, String> mappings = new HashMap<>(3);
        mappings.put(SCInitialPage.recipe, "/recipes/");
        mappings.put(SCInitialPage.mealPlan, "/meal-planner/");
        mappings.put(SCInitialPage.grocery, "/grocery/");
        return mappings;
    }

    public static String getUrl(SCInitialPage initialPage, String id) {
        String nextPath = "";
        if (initialPage != null) {
            HashMap<SCInitialPage, String> urlMaps = getMappings();
            String baseNextPath = "";
            if (initialPage == SCInitialPage.recipe) {
                baseNextPath = urlMaps.get(initialPage) + id;
            } else {
                baseNextPath = urlMaps.get(initialPage);
            }

            nextPath = baseNextPath;
        } else {
            String redirectUrl = SideChefCXP.getInstance().getRedirectUrl();
            if(!redirectUrl.isEmpty()) {
                return redirectUrl;
            }
        }

        return BASE_URL.concat(getPartnerKey()).concat(nextPath);
    }

    public static String getTimestamp() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        Date date = new Date(System.currentTimeMillis());
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("gmt"));
        return simpleDateFormat.format(date);
    }

    public static String getUserName() {
        UserInfo userInfo = SideChefCXP.getInstance().getUserInfo();
        String userId = userInfo.getId();
        return getPartnerKey() + "$" + userId;
    }

    public static String getSign(String userId, String timestamp) {
        String clientKey = getKeys()[1];
        return getSignature(clientKey, userId, timestamp);
    }

    public static String getProfilePayload() {
        SideChefCXP sideChefCXP = SideChefCXP.getInstance();
        UserInfo userInfo = sideChefCXP.getUserInfo();
        String timestamp = getTimestamp();
        String signature = getSign(userInfo.getId(), timestamp);

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("username", getUserName());
            jsonObject.put("appliances", sideChefCXP.getAppliances());
            jsonObject.put("timestamp", timestamp);
            jsonObject.put("signature", signature);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jsonObject.toString();
    }

    public static HashMap<String, Object> getAuthParams() {
        SideChefCXP sideChefCXP = SideChefCXP.getInstance();
        UserInfo userInfo = sideChefCXP.getUserInfo();
        String timestamp = getTimestamp();
        String signature = getSign(userInfo.getId(), timestamp);

        HashMap<String, Object> paramMap = new HashMap<>(5);
        paramMap.put("username", getUserName());
        paramMap.put("appliances", sideChefCXP.getAppliances());
        paramMap.put("timestamp", timestamp);
        paramMap.put("signature", signature);

        return paramMap;
    }

    private static String getSignature(@NonNull String clientKey, @NonNull String userId, @NonNull String timestamp) {
        return Security.hmacSha256(clientKey, userId + timestamp);
    }
}
