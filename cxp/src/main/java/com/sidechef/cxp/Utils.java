package com.sidechef.cxp;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;

import java.util.List;

public class Utils {
    public static String getUserAgent() {
        SideChefCXP cxp = SideChefCXP.getInstance();
        Context context = cxp.getContext();

        String userId = cxp.getUserInfo().getId();
        String appVersion = getAppVersion(context);
        String uuid =  new DeviceUuidFactory(context).getDeviceUuid().toString();
        StringBuilder userAgentSb = new StringBuilder("SideChef CXP")
                .append("/").append(appVersion).append(" ")
                .append(" (Android ").append(Build.MODEL)
                .append(";Android ").append(Build.VERSION.RELEASE).append(") ")
                .append(Authentication.getPartnerKey())
                .append("/").append(uuid)
                .append("/").append(userId);

        return userAgentSb.toString();
    }

    private static String getAppVersion(Context context) {
        try {
            PackageManager manager = context.getPackageManager();
            PackageInfo info = manager.getPackageInfo(context.getPackageName(), 0);
            return info.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }
}
