package com.sidechef.cxp;

import android.util.Log;

import com.sidechef.cxp.network.API;
import com.sidechef.cxp.network.HttpClient;
import com.sidechef.cxp.network.ListResponse;
import com.sidechef.cxp.network.Recipe;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.ResponseBody;
import okhttp3.internal.http.HttpHeaders;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SCAPI {
    private static String TAG = "SCAPI";
    private volatile static SCAPI singleton;
    protected API api;

    private SCAPI() {
        api = HttpClient.getInstance().getRetrofit().create(API.class);
    }

    public static SCAPI getInstance() {
        if (singleton == null) {
            synchronized (SCAPI.class) {
                if (singleton == null) {
                    singleton = new SCAPI();
                }
            }
        }
        return singleton;
    }

    public void getHomePopular(int page, int pageSize, Callback<ListResponse<Recipe>> callback) {
        authenticate(success -> {
            Map<String, Object> paramMap = new HashMap<>(2);
            paramMap.put("page", page);
            paramMap.put("page_size", pageSize);

            Call<ListResponse<Recipe>> call = api.getHomePopular(paramMap);
            call.enqueue(callback);

            Log.d(TAG, "getHomePopular with authenticate success: " + success);
        });
    }

    public void authenticate(AuthenticateCallback callback) {
        if (!SideChefCXP.getInstance().getCookie().isEmpty()) {
            Log.d(TAG, "Cookies exist");
            callback.onComplete(true);
            return;
        }

        HashMap<String, Object> paramMap = Authentication.getAuthParams();
        Call<ResponseBody> call = api.authenticate(paramMap);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.code() == 200) {
                    List<String> cookies = response.raw().priorResponse().headers().values("Set-Cookie");
                    SideChefCXP.getInstance().saveCookies(cookies);

                    String url = response.raw().request().url().toString();
                    if(!url.isEmpty()) {
                        SideChefCXP.getInstance().saveRedirectUrl(url);
                        callback.onComplete(true);

                        Log.d(TAG, "Authenticate with cookies and url" + url);
                        return;
                    }
                }

                Log.d(TAG, "Authenticate complete with " + response.code());
                callback.onComplete(false);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e(TAG, "Authenticate fail");
                callback.onComplete(false);
            }
        });
    }
}
