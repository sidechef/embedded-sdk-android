package com.sidechef.cxp;

public interface AuthenticateCallback {
    void onComplete(boolean success);
}
