package com.sidechef.cxp;

import androidx.appcompat.app.AlertDialog;
import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.util.Log;
import android.webkit.CookieManager;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SCWebView  extends WebView implements AuthenticateCallback {
    private static String TAG = "SCWebView";
    private boolean hasProgress = true;
    private AlertDialog progressView;
    Callback onExternalLink;
    SCInitialPage initialPage;
    // 当前只有initialPage = recipe的时候需要id, 这个时候的id是recipeID
    String initialId;

    public interface Callback {
        void execute(String url);
    }

    public void setOnExternalLink(Callback onExternalLink) {
        this.onExternalLink = onExternalLink;
    }

    public void setInitialPage(SCInitialPage initialPage) {
        setInitialPage(initialPage, null);
    }

    public void setInitialPage(SCInitialPage initialPage, String id) {
        if (initialPage == SCInitialPage.recipe) {
            if (id == null) throw new RuntimeException("Recipe id is required!");

            try {
                Integer.parseInt(id);
            } catch (Exception e) {
                throw new RuntimeException("Invalid recipe id, please use int string!");
            }
        }

        this.initialPage = initialPage;
        this.initialId = id;
    }

    public boolean isHasProgress() {
        return hasProgress;
    }

    public void setHasProgress(boolean hasProgress) {
        this.hasProgress = hasProgress;
    }

    public AlertDialog getProgressView() {
        return progressView;
    }

    public void setProgressView(AlertDialog progressView) {
        this.progressView = progressView;
    }

    public SCWebView(@NonNull Context context) {
        super(context);
    }

    /**
     * create SCWebView with params
     * @param context
     * @param hasProgress disable progress dialog while loading url
     * @param progressView change progress dialog UI
     */
    public SCWebView(@NonNull Context context, boolean hasProgress, AlertDialog progressView) {
        super(context);
        this.hasProgress = hasProgress;
        this.progressView = progressView;
    }

    public SCWebView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        getPropertyFromXml(context, attrs);
    }

    public SCWebView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        getPropertyFromXml(context, attrs);
    }

    public SCWebView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        getPropertyFromXml(context, attrs);
    }

    private void getPropertyFromXml(Context context, AttributeSet attrs) {
        TypedArray ta = context.obtainStyledAttributes(attrs, R.styleable.SCWebview);
        this.hasProgress = ta.getBoolean(R.styleable.SCWebview_hasProgress, true);
        ta.recycle();
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();

        this.changeSettings();
        this.addWebViewClient();

        // authenticate before jump to url
        SCAPI.getInstance().authenticate(this);
    }

    @Override
    public void onComplete(boolean success) {
       
        Map<String, String> headers = new HashMap<>();

        String profilePayload = Authentication.getProfilePayload();
        headers.put("body", profilePayload);

        // load url with authenticate cookies
        String cookies = SideChefCXP.getInstance().getCookie();
        if (success && !cookies.isEmpty()) {

            // update redirect url cookie
            String token = SideChefCXP.getInstance().getCsrftoken();
            String sessionId = SideChefCXP.getInstance().getSessionid();

            CookieManager.getInstance().setCookie(Authentication.BASE_URL, token);
            CookieManager.getInstance().setCookie(Authentication.BASE_URL, sessionId);

            headers.put("Cookie", sessionId + "; " + token);
        } else {
            Log.e(TAG, "LoadUrl without authenticate");
        }

        String url = Authentication.getUrl(this.initialPage, this.initialId);
        this.loadUrl(url, headers);
    }

    private void changeSettings() {
        WebSettings webSettings = getSettings();
        webSettings.setSupportZoom(false);
        webSettings.setBuiltInZoomControls(false);
        webSettings.setDisplayZoomControls(false);
        webSettings.setUserAgentString(Utils.getUserAgent());
        webSettings.setJavaScriptEnabled(true);
        webSettings.setJavaScriptCanOpenWindowsAutomatically(true);
        webSettings.setAllowFileAccess(true);
        webSettings.setDomStorageEnabled(true);
        webSettings.setDatabaseEnabled(true);
        webSettings.setAppCacheEnabled(true);
    }

    private AlertDialog getDefaultProgressDialog() {
        return ProgressHelper.getProgressDialog(this.getContext(), "Loading ...");
    }

    // ensure ProgressView only show once
    private boolean isShowingProgressView = false;

    private void addWebViewClient() {
        if (hasProgress) {
            progressView = progressView != null ? progressView : this.getDefaultProgressDialog();
        }

        setWebViewClient( new WebViewClient(){
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
                String requestUrl = request.getUrl().toString();
                if (!requestUrl.startsWith(Authentication.BASE_URL) && onExternalLink != null) {
                    onExternalLink.execute(requestUrl);
                    return false;
                }
                return super.shouldOverrideUrlLoading(view, request);
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                if (progressView != null && progressView.isShowing()) {
                    progressView.dismiss();
                }
            }

            @Override
            public void onLoadResource(WebView view, String url) {
                super.onLoadResource(view, url);
                if(progressView == null || progressView.isShowing()) return;

                // only show once
                if (!isShowingProgressView) {
                    progressView.show();
                    isShowingProgressView = true;
                }
            }
        });
    }
}
