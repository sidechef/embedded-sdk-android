package com.sidechef.cxp;

public enum SCInitialPage {
    recipe,   // go to /recipes/${id}
    mealPlan, // go to /meal-planner/
    grocery   // go to /grocery/
}
