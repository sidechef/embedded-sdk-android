package com.sidechef.cxp;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;
import android.util.Log;

import androidx.annotation.NonNull;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TimeZone;

public class SideChefCXP {
    private static final String TAG = "SideChefCXP";
    private static final String SP_CONTENT_NAME = "SP_Cookies";
    private static final String SP_CONTENT_KEY = "SP_Cookies_Content";

    private static final String SP_URL_NAME = "SP_URL";
    private static final String SP_URL_KEY = "SP_URL_Content";

    private Context context;
    private String apiKey;
    private UserInfo userInfo;
    private String[] appliances;

    private volatile static SideChefCXP singleton;

    private SideChefCXP() {}

    public static SideChefCXP getInstance() {
        if (singleton == null) {
            synchronized (SideChefCXP.class) {
                if (singleton == null) {
                    singleton = new SideChefCXP();
                }
            }
        }
        return singleton;
    }

    public void configure(@NonNull Context context, @NonNull String apiKey, @NonNull UserInfo userInfo) {
        this.context = context;
        this.apiKey = apiKey;
        this.userInfo = userInfo;

        checkParam();
    }

    public void configure(@NonNull Context context, @NonNull String apiKey, @NonNull UserInfo userInfo, String[] appliances) {
        this.context = context;
        this.apiKey = apiKey;
        this.userInfo = userInfo;
        this.appliances = appliances;

        checkParam();
    }


    private void checkParam() {
        if (TextUtils.isEmpty(apiKey) || null == userInfo || TextUtils.isEmpty(userInfo.getId())) {
            throw new RuntimeException("apiKey and userInfo is required");
        }
    }

    public Context getContext() {
        return context;
    }

    public String getApiKey() {
        return apiKey;
    }

    public UserInfo getUserInfo() {
        return userInfo;
    }

    public String[] getAppliances() {
        return appliances;
    }

    public void setAppliances(String[] appliances) {
        this.appliances = appliances;
    }

    public static boolean isExpires(String expires) {
        Date date = new Date(System.currentTimeMillis());

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("E, dd MMM yyyy HH:mm:ss 'GMT'");
        try {
            Date expiresDate = simpleDateFormat.parse(expires);
            return date.getTime() >= expiresDate.getTime();
        }catch (Exception e) {
            Log.d(TAG, "Parse expires date error");
        }

        return false;
    }

    public String getCookie() {
        List<String> cookies = getCookiesFromSP();

        if (cookies.isEmpty()) return "";

        String sessionidCookie = "";
        String csrftokenCookie = "";
        for (String cookie: cookies) {
            if(cookie.contains("sessionid")) {
                sessionidCookie = cookie;
            }
            if(cookie.contains("csrftoken")) {
                csrftokenCookie = cookie;
            }
        }

        // ensure expiresCookie is valid
        if (sessionidCookie.isEmpty() || csrftokenCookie.isEmpty()) {
            clearCookies();
            return "";
        }

        // clear cookies if expires
        String expires = sessionidCookie.split(";")[1].split("=")[1];
        if (isExpires(expires)) {
            clearCookies();
            return "";
        }

        return sessionidCookie.split(";")[0] + "; " + csrftokenCookie.split(";")[0];
    }

    public String getCsrftoken () {
        List<String> cookies = getCookiesFromSP();
        if (cookies.isEmpty()) return "";

        String csrftokenCookie = "";
        for (String cookie: cookies) {
            if(cookie.contains("csrftoken")) {
                csrftokenCookie = cookie;
            }
        }
        // ensure expiresCookie is valid
        if (csrftokenCookie.isEmpty()) return "";
        return csrftokenCookie.split(";")[0];
    }

    public String getSessionid () {
        List<String> cookies = getCookiesFromSP();
        if (cookies.isEmpty()) return "";

        String sessionidCookie = "";
        for (String cookie: cookies) {
            if(cookie.contains("sessionid")) {
                sessionidCookie = cookie;
            }
        }
        // ensure expiresCookie is valid
        if (sessionidCookie.isEmpty()) return "";
        return sessionidCookie.split(";")[0];
    }

    private void clearCookies() {
        if (this.context != null) return;

        this.context.getSharedPreferences(SP_CONTENT_NAME, Context.MODE_PRIVATE)
                .edit()
                .clear();

        this.context.getSharedPreferences(SP_URL_KEY, Context.MODE_PRIVATE)
                .edit()
                .clear();
    }

    public void saveCookies(List<String> cookies) {
        if (this.context == null || cookies == null) return;

        SharedPreferences sp = this.context.getSharedPreferences(SP_CONTENT_NAME, Context.MODE_PRIVATE);
        Set<String> contentSet = new HashSet<>(cookies);
        sp.edit().putStringSet(SP_CONTENT_KEY, contentSet).apply();
    }

    private List<String> getCookiesFromSP() {
        if (this.context == null) return Collections.emptyList();

        SharedPreferences sp = this.context.getSharedPreferences(SP_CONTENT_NAME, Context.MODE_PRIVATE);
        Set<String> contentSet =  sp.getStringSet(SP_CONTENT_KEY, null);

        if (contentSet != null) return new ArrayList<>(contentSet);

        return Collections.emptyList();
    }

    public void saveRedirectUrl(String url) {
        if (this.context == null || "" == url) return;
        SharedPreferences sp = this.context.getSharedPreferences(SP_URL_NAME, Context.MODE_PRIVATE);
        sp.edit().putString(SP_URL_KEY, url).apply();
    }

    public String getRedirectUrl() {
        if (this.context == null) return "";
        SharedPreferences sp = this.context.getSharedPreferences(SP_URL_NAME, Context.MODE_PRIVATE);
        return sp.getString(SP_URL_KEY, "");
    }
}
